import React from 'react';
import './App.css';
import Fetcher from './Fetcher';
import NewsList from './NewsList';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      list: null,
    };
  }

  componentDidMount() {
    const that = this;

    Fetcher()
      .then((resp) => {
        that.setState({ list: resp });
      });
  }

  render() {
    return (
      <div>
        <NewsList data={this.state.list} />
      </div>
    );
  }
}

export default App;
