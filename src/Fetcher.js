import RSSParser from 'rss-parser';

const Fetcher = () => {
  return new Promise((resolve, reject) => {
    const CORS_PROXY = 'https://cors-anywhere.herokuapp.com/';
    let parser = new RSSParser();

    const interiaRSS = 'http://fakty.interia.pl/polska/feed';
    // const redditRSS = 'https://www.reddit.com/.rss';

    parser.parseURL(CORS_PROXY + interiaRSS, (err, feed) => {
      resolve(feed);
    });
  });
};

export default Fetcher;