import React from 'react';

class NewsList extends React.Component {
  render() {
    return (
      <div>
        {
          this.props.data && 
          <div>
            {
              this.props.data.items.map((item) => {
                return <div key={item.title}>{item.title}</div>;
              })
            }
          </div>
       }
      </div>
    );
  }
};

export default NewsList;
